using System.Collections.Generic;
using UnityEngine;

public class TargetPool : MonoBehaviour
{
    //gameObjectData  GameObject go;  bool busing;
    //Void InitObjectPool;
    //private ListObjectData pObjectList;
    //GameObjectFromPool;
    //class ListObjectData   object dataSource; List<GameObjectData> pData;
    //UnLoadObjectToPool;



    public class TargetData
    {
        public GameObject go;
        public bool bUsing;
        public int id;
    }
    public class ListTargetData
    {
        public Object dataSrc;
        public List<TargetData> pDatas;
    }

    //實例化
    private static TargetPool _instance;
    public static TargetPool InstanceP() { return _instance; }

    private ListTargetData pTargetList;


    //在Start之前就要先處理好
    public void Awake()
    {
        _instance = this;
    }

    public void InitTargetPool(int iCount, Object t)
    {
        pTargetList = new ListTargetData(); //new List<TargetData>();
        pTargetList.dataSrc = t;
        pTargetList.pDatas = new List<TargetData>();
        int i = 0;
        for (i = 0; i < iCount; i++)
        {
            GameObject go = GameObject.Instantiate(t) as GameObject;
            TargetData gdata = new TargetData();
            gdata.go = go;
            gdata.bUsing = false;
            gdata.id = pTargetList.pDatas.Count;
            go.SetActive(false);
            pTargetList.pDatas.Add(gdata);
        }
    }

    public TargetData LoadTargetFromPool()
    {
        int iCount = pTargetList.pDatas.Count;
        int i = 0;
        for(i=0 ; i<iCount ; i++)
        {
            TargetData tData = pTargetList.pDatas[i];
            if (tData.bUsing)
            {
                continue;
            }

            tData.bUsing = true;
            return tData;
        }
        
        return null;
    }
    






}
